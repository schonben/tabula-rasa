# Tabula Rasa WP Theme

This is a WordPress theme build on Timber. This is a blank theme.

### How do I get set up? ###

* Download zip
* Install Timber plug-in
* Install theme in WordPress
* Activate
* Enjoy!
