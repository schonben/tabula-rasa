<?php

/*====================================
=            Hook Library            =
====================================*/

add_action( 'after_setup_theme', 'tabula_setup' );
add_action( 'wp_enqueue_scripts', 'tabula_scripts' );
add_action( 'after_setup_theme', 'register_my_menu' );

add_filter('timber_context', 'tabularasa_context');


/*-----  End of Hook Library  ------*/

function tabula_setup() {
    // Theme Supports
    add_theme_support( 'custom-header' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'menus' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'custom-background' );

    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    // Translation Support
    //load_theme_textdomain( 'tabularasa', get_stylesheet_directory() . '/languages' );
    //$locale = get_locale();
    //$locale_file = get_template_directory() . "/languages/$locale.php";
    //if ( is_readable( $locale_file ) )
    //    require_once( $locale_file );

    // Image Sizes
    // add_image_size( 'nn_list', 730, 288, true );

}
function tabula_scripts() {
    wp_register_style("bootstrap",get_template_directory_uri()."/dist/bootstrap.min.css",array(),"4.0");
    wp_register_script("bootstrap",get_template_directory_uri()."/dist/bootstrap.min.js",array("jquery"),"4.0");

    wp_register_style("theme",get_template_directory_uri()."/assets/css/style.css",array("bootstrap"),"0.1");
    wp_register_script("theme",get_template_directory_uri()."/assets/js/tabularasa.js",array("jquery","bootstrap"),"0.1");

    wp_enqueue_style("theme");
    wp_enqueue_script("theme");
}

function register_my_menu() {
    register_nav_menu( 'primary', __( 'Primary Menu', 'tabularasa' ) );
}

function tabularasa_context($data){
    /* Now, you add a Timber menu and send it along to the context. */
    $data['menu'] = new TimberMenu('primary'); // This is where you can also send a Wordpress menu slug or ID
    $data['header'] = get_header_image();
    //$data['sidebarleft'] = Timber::get_widgets('sidebar-left');
    //$data['sidebarright'] = Timber::get_widgets('sidebar-right');

    return $data;
}
function tabularasa_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Left Widget Area', 'tabularasa' ),
        'id'            => 'sidebar-left',
        'description'   => __( 'Add widgets here to appear on the left side.', 'tabularasa' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Right Widget Area', 'tabularasa' ),
        'id'            => 'sidebar-right',
        'description'   => __( 'Add widgets here to appear on the right side.', 'tabularasa' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
//add_action( 'widgets_init', 'tabularasa_widgets_init' );
