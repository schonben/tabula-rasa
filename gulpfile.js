// Load Node Modules/Plugins
var gulp = require('gulp');
var sass = require('gulp-sass');
//var rename = require('gulp-rename');
var minifyCss = require('gulp-clean-css');

gulp.task('styles', function() {
    gulp.src('./scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        //.pipe(minifyCss({compatibility: 'ie9'}))
        .pipe(gulp.dest('./assets/css/'))
});

// Watch task
gulp.task('default',function() {
    // run task initially, after that watch
    gulp.start('styles');
    gulp.watch('./scss/*.scss',['styles']);
});

