<?php
/*
Template Name: Frontpage
*/

// Set up page data
$data = Timber::get_context();

$data['post'] = new TimberPost();
$data['posts'] = Timber::get_posts("post_type=post");
$data['pagination'] = Timber::get_pagination();

Timber::render("front.twig",$data);
